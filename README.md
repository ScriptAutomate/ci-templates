# ci-templates

gitlab-ci templates

# docker

kaniko builder templates

# lint

pre-commit

# test

test jobs for windows, macos, linux

# build

build jobs for windows, macos, linux

# upload

upload job

# release

release job
